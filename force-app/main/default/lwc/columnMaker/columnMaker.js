import { api, LightningElement } from 'lwc';

export default class ColumnMaker extends LightningElement 
{
    @api tabIndex

    get columnSetter()
    {
        return this.tabIndex%2 ==0 ? true:false;
    }
}