import { api, LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import createStudentRecord from "@salesforce/apex/StudentRecordHandler.createStudentRecord";
import getFieldSetMembers from "@salesforce/apex/StudentRecordHandler.getFieldSetMembers";
import searchForRecord from "@salesforce/apex/StudentRecordHandler.searchForRecord";
import updateStudentRecord from "@salesforce/apex/StudentRecordHandler.updateStudentRecord";

export default class StudentDetails extends NavigationMixin(LightningElement) {

    @track objStudents = {};
    @track listOfFields = [];
    @track objStudentForUpdate = {};
    @track objListOfFieldsForCancel = {};
    @api recordId;
    @track disabled;
    @api objectName;
    @api fieldSetName;

    connectedCallback() {
        this.fieldMemberHandler();
        if (this.recordId == null) {
            this.buttonLabel = "Save";
        }
        else {

            this.buttonLabel = "Edit";
            this.disabled = true;
        }
    }


    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    navigateHandler(recId) {
        this.accountHomePageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recId,
                objectApiName: objectName,
                actionName: 'view'
            }
        }
        this[NavigationMixin.Navigate](this.accountHomePageRef);
    }

   //method for fetching the fields for input dynamically from Fieldsets. 
    fieldMemberHandler() {
        getFieldSetMembers({
            fieldSetName:  this.fieldSetName,
            ObjectName: this.objectName
        })
            .then(response => {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].type == "BOOLEAN") {
                        response[i].type = "checkbox";
                    }
                    else if (response[i].type == "STRING") {
                        response[i].type = "text";
                    }
                    else if (response[i].type == "DOUBLE" || response[i].type == "PERCENT") {
                        response[i].type = "number";
                    }
                    else if (response[i].type == "TEXTAREA") {
                        response[i].textArea = true;
                    }
                    else if (response[i].type == "URL") {
                        response[i].url = true;
                      
                    }

                }
                this.listOfFields = response;
            }).catch(error => {
                console.error(error);
            })

            this.searchForRecordHandler();
    }

    // method for storing the input data
    inputHandler(event) {
        let fieldName = event.target.name;
        if (event.target.type == 'checkbox') {
            this.objStudents[fieldName] = event.target.checked;
        }
        else
        {
            this.objStudents[fieldName] = event.target.value;
        }
        
        if (this.recordId != null) {
            if (event.target.type == 'checkbox') {
                this.objStudentForUpdate[fieldName] = event.target.checked;
            }
           
            else {
                this.objStudentForUpdate[fieldName] = event.target.value;
            }
        }

        var webRegex =  /^(https?:\/\/)?(www\.)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\.)+[\w]{2,}(\/\S*)?$/;
        this.template.querySelectorAll('lightning-input').forEach(element => 
        {
            if(element.type == 'date' &&  event.target.name == "BirthDate__c" && event.target.value == null)
            {
                element.value = '';
                element.setCustomValidity('');
                element.reportValidity(); 
               


            }
            else if(element.type == 'date' &&  event.target.name == "AdmissionDate__c" && event.target.value == null )
            {
                element.value = '';
                element.setCustomValidity('');
                element.reportValidity(); 
             
            }
            else if(element.name == "SocialProfile__c" &&  event.target.name == "SocialProfile__c" && event.target.value == null )
            {

                //this.objStudents.SocialProfile__c = '';
                element.value = '';
                element.setCustomValidity('');
                element.reportValidity(); 
             
            }

        });

    }

    // method for inserting the new student record to database
    createStudentRecordHandler() {
     console.log(this.objStudents);
        if(this.objStudents.Name != null || this.objStudents.Name !='')
        {
            createStudentRecord({ studentsObj: this.objStudents })
                .then(result => {
                    if (result.Id != null && this.recordId == null) {
                        this.showNotification('Success', `${result.Name} Student Created Successfully`, 'success');
                        this.navigateHandler(result.Id);
                    }
                    else {
                        this.showNotification('Error', "Failed Insert", 'error');
                    }
                }).catch(error => {
    
                    this.showNotification('Error', error.body.message, 'error');
                    console.error(error);
                });

        }
        
    }

    //method for fetching the data of the existing student record 
    searchForRecordHandler() {
        if (this.recordId != null) {
            searchForRecord({ studentId: this.recordId,
                              fieldSetName: this.fieldSetName,
                              objectName: this.objectName })
                .then(res => {
                    this.objListOfFieldsForCancel = Object.assign({}, res[0]);
                    this.objStudentForUpdate = Object.assign({}, res[0]);
                    for(var i= 0; this.listOfFields.length;i++)
                    {
                        if(res[0].hasOwnProperty(this.listOfFields[i].apiName))
                        {
                            this.listOfFields[i].val = res[0][this.listOfFields[i].apiName];   
                        }
                    }

                }).catch(error => {
                    console.error(error);
                })
        }
    }

    // Method for Updating the student record
    updateStudentRecordHandler() {
       
        updateStudentRecord({ studentsObj: this.objStudentForUpdate })
            .then(updateRes => {
                if (updateRes.Id != null) {  
                    console.log(updateRes.Id, updateRes.Name , `${updateRes.Name} Student Updated Successfully`);     
                    this.showNotification('Success', `${updateRes.Name} Student Updated Successfully`, 'success');
                }
                else {
                    this.showNotification('Error', "Failed Update", 'error');
                }
            }).catch(error => {
                this.showNotification('Error', error.body.message, 'error');
                this.disabled = false;
                this.buttonLabel = "Update";
                console.error(error);
            });
    }

    //Method for clearing the entered values from the input fields
    resetHandler(event) {
        if (this.recordId == null) {
            this.objStudents = {};
            this.template.querySelectorAll('lightning-input').forEach(element => {
                if (element.type === 'checkbox' || element.type === 'checkbox-button') {
                    element.checked = false;
                }
                else {
                    element.value = null;
                }
            });
        }
        else {

            // handling the cancel event in update mode.. 
            // During update if cancel button is pressed the
            // older values should get reflected in the field

            let copyOfListofFields = this.listOfFields;

            for (let i = 0; i < copyOfListofFields.length; i++) {
                if (this.objListOfFieldsForCancel.hasOwnProperty(copyOfListofFields[i].apiName)) {
                    
                    copyOfListofFields[i].val = this.objListOfFieldsForCancel[copyOfListofFields[i].apiName];
                } else {
                    copyOfListofFields[i].val = '';
                }
            }
            this.listOfFields = copyOfListofFields;
            
            this.template.querySelectorAll('lightning-input').forEach(element => {
                element.value = this.objListOfFieldsForCancel.hasOwnProperty(element.name) ? this.objListOfFieldsForCancel[element.name] : '';

                if (element.type == 'checkbox') {
                    element.checked = this.objListOfFieldsForCancel[element.name];
                }
                if(element.type == 'email')
                {
                    element.setCustomValidity('');
                    element.reportValidity();
                }
            });
            this.objStudentForUpdate = this.objListOfFieldsForCancel;
            this.buttonLabel = "Edit";
            this.disabled = true;
        }
    }

// handler method for Save, Edit and Update operations
    buttonHandler(event) {
        if (this.recordId == null) {
            if (event.target.label == "Save") {
                this.createStudentRecordHandler();
            //    this.createStudentRecordValidation();
            }
        }
        else {
            if (event.target.label == "Update") {
                this.disabled = true;
                this.buttonLabel = "Edit";
                this.updateStudentRecordHandler();
               // this.navigateHandler(this.recordId);
            }
            else if (event.target.label == "Edit") {
                this.disabled = false;
                this.buttonLabel = "Update";
            }
        }
    }
}