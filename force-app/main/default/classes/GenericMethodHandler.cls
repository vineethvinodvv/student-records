public with sharing class GenericMethodHandler {
    
    @AuraEnabled
    public static Object createStudentRecord(Sobject studentsObj){
        try {
           
            if(studentsObj != null)
            {
                Insert studentsObj;
                return new Map<String, String>{'Id'=> studentsObj.Id};
            }
            else {
                return 'Insert Failed';
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }


/*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :   Handler Method for Updating record with input from the LWC page to Database
*/
    @AuraEnabled
    public static Object updateStudentRecord(Sobject studentsObj){
        try {
            
            if(studentsObj != null)
            {  
                Update studentsObj;
                return new Map<String, String>{'Id'=> studentsObj.Id};
            }
            else {
                return 'Update Failed';
            } 
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :  Method for fetching the fieldSets of the object.
*/
    @AuraEnabled
    public static List<FieldWrapper> getFieldSetMembers(String fieldSetName, String ObjectName)
    {
        List<FieldWrapper> listOfFieldWrapper = new List<FieldWrapper>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        
        List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields(); 

        for (Schema.FieldSetMember objFM : fieldSetMemberList) 
        {
            listOfFieldWrapper.add(new FieldWrapper( objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), String.valueOf(objFM.getType()), objFM.getDBRequired()));
        }
        return listOfFieldWrapper;
    }  

/*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :   Wrapper class for adding all the objects for creating the fieldset list
*/

    public class FieldWrapper
    {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String apiName {get;set;}
        @AuraEnabled public Boolean isRequired {get;set;}
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public Boolean isDBRequired {get;set;}

        FieldWrapper(){}

        FieldWrapper(String label, String apiName, Boolean isRequired, String type, Boolean isDBRequired)
        {
            this.label = label;
            this.apiName = apiName;
            this.isDBRequired = isDBRequired;
            this.isRequired  = isRequired;
            this.type = type;
        }
   }

   /*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :   Method used for getting the values of the record of the given recordId
*/

   @AuraEnabled
   public static List<Sobject> searchForRecord(String studentId, String fieldSetName, String objectName){
       try 
       {
            String query = 'SELECT ';
            for(FieldWrapper objFieldWrapper : getFieldSetMembers(fieldSetName,objectName)) 
            {
                query += objFieldWrapper.apiName + ', ';
            }
            String std = '\''+ studentId +'\'';
           
            query += 'Id FROM '+objectName+'  WHERE Id = '+std+'' ;            
            List<Sobject> listOfStudent = Database.query(query);
            return listOfStudent;
    
           
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }
}
